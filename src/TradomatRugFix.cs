using System.Reflection;
using HarmonyLib;
using Vintagestory.API.Common;
using System;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace TradomatRugFix
{
    public class PatchBERug : ModSystem
    {
        private Harmony _harmony;
        private static ICoreServerAPI _api;
        private static ModSystemBlockReinforcement _reinforce;

        public override void StartServerSide(ICoreServerAPI api)
        {
            _api = api;
            _reinforce = _api.ModLoader.GetModSystem<ModSystemBlockReinforcement>();
            _harmony = new Harmony("TradomatRugFix");
            Type BERug = AccessTools.TypeByName("tradeomat.src.TradeomatBlock.Rug.BERug");
            Type rugBlock = AccessTools.TypeByName("tradeomat.src.TradeomatBlock.Rug.Rug");
            Type matBlock = AccessTools.TypeByName("tradeomat.src.TradeomatBlock.TradeBlock");

            MethodInfo originalOnReceivedClientPacket = AccessTools.Method(BERug, "OnReceivedClientPacket");
            HarmonyMethod postOnReceivedClientPacket = new HarmonyMethod(typeof(PatchBERug).GetMethod(nameof(PostOnReceivedClientPacket)));
            _harmony.Patch(originalOnReceivedClientPacket, postfix: postOnReceivedClientPacket);

            MethodInfo originalFromTreeAttributes = AccessTools.Method(BERug, "FromTreeAttributes");
            HarmonyMethod postFromTreeAttributes = new HarmonyMethod(typeof(PatchBERug).GetMethod(nameof(PostFromTreeAttributes)));
            _harmony.Patch(originalOnReceivedClientPacket, postfix: postFromTreeAttributes);

            TradeomatFixConfig tradeomatFixConfig = api.LoadModConfig<TradeomatFixConfig>("tradeomatfix.json");

            if (tradeomatFixConfig != null && tradeomatFixConfig.AutoReinforce)
            {
                MethodInfo originalPlace = AccessTools.Method(rugBlock, "TryPlaceBlock");
                HarmonyMethod place = new HarmonyMethod(typeof(PatchBERug).GetMethod(nameof(PostTryPlaceBlockRug)));
                _harmony.Patch(originalPlace, postfix: place);

                MethodInfo originalPlaceMat = AccessTools.Method(matBlock, "TryPlaceBlock");
                HarmonyMethod placeMat = new HarmonyMethod(typeof(PatchBERug).GetMethod(nameof(PostTryPlaceBlockRug)));
                _harmony.Patch(originalPlaceMat, postfix: placeMat);
            }

        }

        public static void PostOnReceivedClientPacket(int[] ___prices)
        {
            for (int i = 0; i < ___prices.Length; i++)
            {
                ___prices[i] = Math.Abs(___prices[i]);
            }
        }

        public static void PostFromTreeAttributes(int[] ___prices)
        {

            for (int i = 0; i < ___prices.Length; i++)
            {
                ___prices[i] = Math.Abs(___prices[i]);
            }
        }

        static public void PostTryPlaceBlockRug(IWorldAccessor world, IPlayer byPlayer, ItemStack itemstack, BlockSelection blockSel, ref string failureCode, bool __result)
        {
            if (__result && _reinforce != null)
            {
                _reinforce.StrengthenBlock(blockSel.Position, byPlayer, 50);
            }
        }

        static public void PostTryPlaceBlockMat(IWorldAccessor world, IPlayer byPlayer, ItemStack itemstack, BlockSelection blockSel, ref string failureCode, bool __result)
        {
            if (__result && _reinforce != null)
            {
                _reinforce.StrengthenBlock(blockSel.Position, byPlayer, 50);
            }
        }
    }
}